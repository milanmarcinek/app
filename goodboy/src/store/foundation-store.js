import { createSlice } from "@reduxjs/toolkit";

const shelterSlice = createSlice({
    name: 'shelters',
    initialState: {sheltersList: []},
    reducers: {
        setListOfShelters(state, action) {
            state.sheltersList = [...action.payload.list];
        }
    }
});

export const shelterActions = shelterSlice.actions;

export default shelterSlice;