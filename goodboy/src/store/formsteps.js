import { createSlice } from "@reduxjs/toolkit";

const formSteps = createSlice({
    name: 'formSteps',
    initialState: {
        step: 'choose',
        header: 'Vyberte si možnosť, ako chcete pomôcť',
        first: 'actualStep',
        second: 'step',
        third: 'step',
        isShelter: false,
        priceActive: '',
        isPriceSet: false,
        isUserSet: false,
        sendContribution: false,
        allowUserInfo: false,
    },
    reducers: {
        setStep (state, action) {
            state.step = action.payload.step;
        },
        setHeaderText(state, action) {
            state.header = action.payload.header;
        },
        setFirstStep(state, action) {
            state.first = action.payload.first;
        },
        setSecondStep(state, action) {
            state.second = action.payload.second;
        },
        setThirdStep(state, action) {
            state.third = action.payload.third;
        },
        setIsShelter(state, action) {
            state.isShelter = action.payload.isShelter;
        },
        setActivePrice(state, action) {
            state.priceActive = action.payload.priceActive;
        },
        setIsPriceSet(state, action) {
            state.isPriceSet = action.payload.isPriceSet;
        },
        setIsUserSet(state, action) {
            state.isUserSet = action.payload.isUserSet;
        },
        setSendContribution(state, action) {
            state.sendContribution = action.payload.sendContribution;
        },
        setUserInfo(state, action) {
            state.allowUserInfo = action.payload.allowUserInfo;
        },
    }
});

export const formStepsActions = formSteps.actions;

export default formSteps;