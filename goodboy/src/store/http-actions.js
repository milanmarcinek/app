import { shelterActions } from './foundation-store';
import { modalActions } from './modal-store';

export const getShelters = () => {
    return async dispatch => {
        fetch('https://frontend-assignment-api.goodrequest.com/api/v1/shelters')
        .then(res => res.json())
        .then(data => {
            dispatch(shelterActions.setListOfShelters({
                list: data.shelters,
            }));
        });
    };
}; 

export const sendContribution = (user) => {
    return async dispatch => {
        fetch('https://frontend-assignment-api.goodrequest.com/api/v1/shelters/contribute', {
            method: 'POST',
            body: JSON.stringify({
                firstName: user.name,
                lastName: user.surname,
                email: user.email,
                value: user.price,
                phone: user.phonenumber,
                shelterID: user.shelterID,
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => {
            dispatch(modalActions.setMessage({message: data.messages[0].message}));
            dispatch(modalActions.setIsVisible({isVisible: true}));
        });
    };
};