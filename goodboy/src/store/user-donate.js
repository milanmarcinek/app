import { createSlice } from "@reduxjs/toolkit";

const userDonate = createSlice({
    name: 'userActions',
    initialState: {
        foundation: 'Chcem finančne prispieť celej nadácií',
        shelter: '',
        shelterID: 0,
        price: 0,
    },
    reducers: {
        setFoundation(state, action) {
            state.foundation = action.payload.foundation;
        },
        setShelter(state, action) {
            state.shelter = action.payload.shelter;
        },
        setShelterID(state, action) {
            state.shelterID = action.payload.shelterID;
        },
        setPrice(state, action) {
            state.price = action.payload.price;
        },
    }
});

export const userActions = userDonate.actions;

export default userDonate;