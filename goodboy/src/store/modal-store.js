import { createSlice } from "@reduxjs/toolkit";

const modalStore = createSlice({
    name: 'modal',
    initialState: {
        message: '',
        isVisible: false,
    },
    reducers: {
        setMessage(state, action) {
            state.message = action.payload.message;
        },
        setIsVisible(state, action) {
            state.isVisible = action.payload.isVisible;
        },
    }
});

export const modalActions = modalStore.actions;

export default modalStore;