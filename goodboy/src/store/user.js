import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
    name: 'user',
    initialState: {
        name: '',
        surname: '',
        email: '',
        phonenumber: '+421',
    },
    reducers: {
        setName(state, action) {
            state.name = action.payload.name;
        },
        setSurname(state, action) {
            state.surname = action.payload.surname;
        },
        setEmail(state, action) {
            state.email = action.payload.email;
        },
        setPhoneNumber(state, action) {
            state.phonenumber = action.payload.phonenumber;
        },

    }
});

export const userInfoActions = userSlice.actions;

export default userSlice;