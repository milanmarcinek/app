import { configureStore } from '@reduxjs/toolkit';
import formSteps from './formsteps';
import shelterSlice from './foundation-store';
import modalStore from './modal-store';
import userSlice from './user';
import userDonate from './user-donate';

const store = configureStore({
    reducer: {shelterSlice: shelterSlice.reducer, 
              userDonate: userDonate.reducer,
              formSteps: formSteps.reducer,
              userSlice: userSlice.reducer,
              modalStore: modalStore.reducer, }
});

export default store;