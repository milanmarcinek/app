import styles from './Footer.module.css';
import nameicon from '../../assets/images/name.svg';
import dogicon from '../../assets/images/dog.svg';
import React from 'react';

const Footer = () => {
    return (
        <React.Fragment>
        <div className={styles.footer}>
            <span className={styles.separator}></span>
            <div className={styles.logo}>
                <img className={styles.name} src={dogicon} alt="name"/>
                <img className={styles.dog} src={nameicon} alt="dog"/>
            </div>
            <div className={styles.first}>
            <div className={styles.about_info1}>
                Nadácia Good boy
            </div>
            <div className={styles.aboutmore_info1}>
                O projekte <br/>Ako na to <br/>Kontakt
            </div>
            </div>
            <div className={styles.second}>
            <div className={styles.about_info2}>
                Nadácia Good boy
            </div>
            <div className={styles.aboutmore_info2}>
                Lorem ipsum dolor sit amet, consectetur<br/> adipiscing elit. Vivamus in interdum<br/> ipsum, sit amet. 
            </div>
            </div>            
            <div className={styles.third}>
            <div className={styles.about_info3}>
                Nadácia Good boy
            </div>
            <div className={styles.aboutmore_info3}>
                Lorem ipsum dolor sit amet, consectetur<br/> adipiscing elit. Vivamus in interdum<br/> ipsum, sit amet. 
            </div>
            </div>
        </div>
        </React.Fragment>
    );
}

export default Footer;