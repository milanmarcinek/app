import styles from './Main.module.css';
import Header from '../header/Header';
import SideImage from '../side/SideImage';
import React from 'react';
import ChooseFoundation from '../forms/ChooseFoundation';
import Donor from '../forms/Donor';
import Result from '../forms/Result';
import { useSelector } from 'react-redux';



const Main = () => {

    const step = useSelector(state => state.formSteps.step);

    return (
        <div className={styles.main}>
            <Header/>
            <SideImage/>
            {step === 'choose' ? <ChooseFoundation/> : ''}
            {step === 'user' ? <Donor/> : ''}
            {step === 'result' ? <Result/> : ''}

        </div>
    );
};

export default Main;