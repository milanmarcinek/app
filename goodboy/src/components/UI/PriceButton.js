const PriceButton = (props) => {

    const setPriceHandler = () => {
        props.setPrice(props.number);
    };

    return (
        <button className={props.className} type="button" onClick={setPriceHandler}>{props.number} €</button>
    );
};

export default PriceButton;