import styles from './ResultItem.module.css';

const ResultItem = (props) => {
    return (
        <div className={styles.resultitem}>
            <p className={styles.name}>{props.headline}</p>
            <p className={styles.text}>{props.text}</p>
        </div>
    );
};

export default ResultItem;