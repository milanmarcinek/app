import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styles from './Header.module.css';
import { formStepsActions } from '../../store/formsteps';

const Header = () => {

    const { header, step, first, second, third } = useSelector(state => state.formSteps);
    const dispatch = useDispatch();

    useEffect( () => {
        switch (step) {
            case 'choose':
                dispatch(formStepsActions.setFirstStep({first: true}));
                dispatch(formStepsActions.setSecondStep({second: false}));
                break;
            case 'user':
                dispatch(formStepsActions.setFirstStep({first: false}));
                dispatch(formStepsActions.setSecondStep({second: true}));
                dispatch(formStepsActions.setThirdStep({third: false}));
                break;
            case 'result':
                dispatch(formStepsActions.setSecondStep({second: false}));
                dispatch(formStepsActions.setThirdStep({third: true}));
                break;
            default:
                break;
        }
    }, [step, dispatch])

    return (
        <div className={styles.header}>
            <div className={styles.steps}>
                <div className={(first === true ? styles.actualStep : styles.step)}></div>
                <div className={(second === true ? styles.actualStep : styles.step)}></div>
                <div className={(third === true ? styles.actualStep : styles.step)}></div>
            </div>
            <p className={styles.mainname}>
                {header}
            </p>
        </div>
    );
};

export default Header;