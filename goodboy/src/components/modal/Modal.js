import React from 'react';
import BackgroundModal from './BackgroundModal';
import styles from './Modal.module.css';
import ReactDOM from 'react-dom';
import { useDispatch } from 'react-redux';
import { modalActions } from '../../store/modal-store';

const Modal = (props) => {

    const dispatch = useDispatch();

    const cancelModal = () => {
        dispatch(modalActions.setIsVisible({isVisible: false}));
    };    

    return ReactDOM.createPortal(
        <React.Fragment>
            <BackgroundModal/>
            <div className={styles.window}>
                <h1>{props.message}</h1>
                <button type="button" onClick={cancelModal}>Zavrieť</button>
            </div>
        </React.Fragment>,
        document.getElementById('modal'));
};

export default Modal;