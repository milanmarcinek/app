import styles from './BackgroundModal.module.css';

const BackgroundModal = () => {
    return (
        <div className={styles.background}></div>
    );
};

export default BackgroundModal;