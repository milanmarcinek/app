import styles from './ChooseFoundation.module.css';
import walleticon from '../../assets/images/wallet.svg';
import { useDispatch, useSelector } from 'react-redux';
import { userActions } from '../../store/user-donate';
import { formStepsActions } from '../../store/formsteps';
import PriceButton from '../UI/PriceButton';

const ChooseFoundation = () => {
    
    const listOfShelters = useSelector(state => state.shelterSlice.sheltersList);
    const { shelter, price, } = useSelector(state => state.userDonate);
    const {isShelter, priceActive, isPriceSet} = useSelector(state => state.formSteps);
    const dispatch = useDispatch();

    const foundationHandler = () => {
        dispatch(userActions.setFoundation({ foundation: 'Chcem finančne prispieť celej nadácií' }));
        dispatch(formStepsActions.setIsShelter({isShelter: false}));
        dispatch(userActions.setShelter({shelter: ''}));

    };

    const shelterHandler = () => {
        dispatch(userActions.setFoundation({ foundation: 'Chcem finančne prispieť útulku' }));
        dispatch(formStepsActions.setIsShelter({isShelter: true}));
    };

    const chooseShelterHandler = (data) => {
        dispatch(userActions.setShelter({ shelter: data.target.value }));
        const shelterID = listOfShelters.filter(item => item.name === data.target.value);
        dispatch(userActions.setShelterID({shelterID: shelterID[0].id}));
    };

    const setPriceHandler = (data) => {
        dispatch(userActions.setPrice({ price: data }));
        dispatch(formStepsActions.setActivePrice({priceActive: data}));
        dispatch(formStepsActions.setIsPriceSet({isPriceSet: true}));
    };

    const setPriceHandlerMore = (data) => {
        dispatch(userActions.setPrice({ price: data.target.value }));
        dispatch(formStepsActions.setActivePrice({priceActive: data.target.value}));
        if (data.target.value > 0) {
            dispatch(formStepsActions.setIsPriceSet({isPriceSet: true}));
        } else {
            dispatch(formStepsActions.setIsPriceSet({isPriceSet: false}));
        }

    };

    const formSubmitHandler = (event) => {
        event.preventDefault();
        dispatch(formStepsActions.setStep({ step: 'user' }));
        dispatch(formStepsActions.setHeaderText({header: 'Potrebujeme od Vás zopár informácií'}));
    }



    return (
        <div className={styles.choose}>
            <div className={styles.mainchoose}>
                <div className={`${isShelter === true ? styles.active : ''} ${styles.shelter}`} onClick={shelterHandler}>
                    <img src={walleticon} alt="wallet"/>
                    <p>Chcem finančne prispieť konkrétnemu útulku</p>
                </div>
                <div className={`${isShelter === true ? '' : styles.active} ${styles.foundation}`} onClick={foundationHandler}>
                    <img src={walleticon} alt="paw"/>
                    <p>Chcem finančne prispieť celej nadácii</p>
                </div>
            </div> 
            <form onSubmit={formSubmitHandler}>
                <div className={styles.label}>
                    <p className={styles.about}>O projekte</p>
                    <p className={styles.no}>Nepovinné</p>
                </div>
                <div className={styles.select}>
                    <label>Útulok</label>
                    <select onChange={chooseShelterHandler} value={shelter}>
                        <option disabled value="">Vyberte útulok zo zoznamu</option>
                        {listOfShelters.map(item => (
                            <option value={item.name} key={item.id}>{item.name}</option>
                        ))}
                    </select>
                </div>
                <p className={styles.price}>Suma, ktorou chcem prispieť</p>
                <div className={styles.pricebuttons}>
                    <PriceButton className={` ${priceActive === "5" ? styles.activeButton : styles.deactiveButton} ${styles.button}  `} number="5" setPrice={setPriceHandler}/>
                    <PriceButton className={` ${priceActive === "10" ? styles.activeButton : styles.deactiveButton} ${styles.button} `} number="10" setPrice={setPriceHandler}/>
                    <PriceButton className={` ${priceActive === "20" ? styles.activeButton : styles.deactiveButton} ${styles.button} `} number="20" setPrice={setPriceHandler}/>
                    <PriceButton className={` ${priceActive === "30" ? styles.activeButton : styles.deactiveButton} ${styles.button} `} number="30" setPrice={setPriceHandler}/>
                    <PriceButton className={` ${priceActive === "50" ? styles.activeButton : styles.deactiveButton} ${styles.button} `} number="50" setPrice={setPriceHandler}/>
                    <PriceButton className={` ${priceActive === "100" ? styles.activeButton : styles.deactiveButton} ${styles.button} `} number="100" setPrice={setPriceHandler}/>
                    <div className={` ${styles.input}  `}>
                        <input type="text" value={price} onChange={setPriceHandlerMore}/>
                        <p>€</p>
                    </div>
                </div>
                <button type="submit" className={`${isPriceSet === true ? styles.submit : styles.deactiveNextStepButton}`} disabled={!isPriceSet}>Pokračovať</button>
            </form>
        </div>
    );
};

export default ChooseFoundation;