import { useDispatch, useSelector } from 'react-redux';
import styles from './Donor.module.css';
import { formStepsActions } from '../../store/formsteps';
import { userInfoActions } from '../../store/user';
import slovakiaflag from '../../assets/images/slovakiaflag.svg'
import czechflag from '../../assets/images/czechflag.svg'
import { useEffect } from 'react';


const Donor = () => {

    const { name, surname, email, phonenumber} = useSelector(state => state.userSlice);
    const { isUserSet } = useSelector(state => state.formSteps);

    const dispatch = useDispatch();

    const setNameHandler = (data) => {
        dispatch(userInfoActions.setName({name: data.target.value}));
    };

    const setSurnameHandler = (data) => {
        dispatch(userInfoActions.setSurname({surname: data.target.value}));
    };

    const setEmailHandler = (data) => {
        dispatch(userInfoActions.setEmail({email: data.target.value}));
    };

    const setPhoneNumberHandler = (data) => {
        dispatch(userInfoActions.setPhoneNumber({phonenumber: data.target.value}));
    };

    const formSubmitHandler = (event) => {
        event.preventDefault();
        dispatch(formStepsActions.setStep({ step: 'result' }));
        dispatch(formStepsActions.setHeaderText({header: 'Skontrolujte si zadané údaje'}));

    }

    const backBtnHandler = () => {
        dispatch(formStepsActions.setStep({ step: 'choose' }));
        dispatch(formStepsActions.setHeaderText({header: 'Vyberte si možnosť, ako chcete pomôcť '}));

    }

    useEffect(() => {
        if (surname.length > 2 && email.includes('@') && (phonenumber.includes('+421') || phonenumber.includes('+420'))) {
            dispatch(formStepsActions.setIsUserSet({isUserSet: true}));
        }
    }, [surname, email, phonenumber, dispatch]);

    return (
        <div className={styles.form}>
            <p className={styles.about}>O vás</p>
            <form onSubmit={formSubmitHandler}>
                <div className={styles.input}>
                    <label>Meno</label>
                    <input placeholder="Zadajte Vaše meno" 
                           type="text" value={name} 
                           onChange={setNameHandler} 
                           minLength="2" 
                           maxLength="30"/>
                </div>
                <div className={styles.input}>
                    <label>Priezvisko</label>
                    <input placeholder="Zadajte Vaše priezvisko" 
                           type="text" 
                           value={surname} 
                           onChange={setSurnameHandler} 
                           minLength="2" 
                           maxLength="30" 
                           required/>
                </div>
                <div className={styles.input}>
                    <label>E-mailová adresa</label>
                    <input placeholder="Zadajte Váš e-mail" 
                           type="email" 
                           value={email} 
                           onChange={setEmailHandler} 
                           required/>
                </div>
                <div className={styles.input}>
                    <label>Telefónne číslo</label>
                    <div className={styles.flag}>
                        <img src={`${phonenumber.includes("+421") ? slovakiaflag : czechflag}`} alt="flag"/>
                        <input placeholder="Zadajte Vaše číslo" 
                           type="text" 
                           value={phonenumber} 
                           onChange={setPhoneNumberHandler} 
                           required/>
                    </div>
                </div>
                <div className={styles.buttons}>
                    <button className={styles.back} 
                            type="button" 
                            onClick={backBtnHandler}>
                                Späť
                    </button>
                    <button className={`${isUserSet === true ? styles.active : styles.send}`} 
                            type="submit" 
                            disabled={!isUserSet}>
                                Pokračovať
                    </button>
                </div>
            </form>
            
        </div>
    );
};

export default Donor;