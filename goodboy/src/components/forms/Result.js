import { useDispatch, useSelector } from 'react-redux';
import styles from './Result.module.css';
import { formStepsActions } from '../../store/formsteps';
import ResultItem from '../UI/ResultItem';
import { sendContribution } from '../../store/http-actions';
import Modal from '../modal/Modal';
import React from 'react';

const Result = () => {

    const dispatch = useDispatch();

    const backBtnHandler = () => {
        dispatch(formStepsActions.setStep({ step: 'user' }));
        dispatch(formStepsActions.setHeaderText({header: 'Potrebujeme od Vás zopár informácií'}));
        dispatch(formStepsActions.setUserInfo({allowUserInfo: false}));

    }

    const { name, surname, email, phonenumber} = useSelector(state => state.userSlice);
    const { price, shelter, foundation } = useSelector(state => state.userDonate);
    const { allowUserInfo } = useSelector(state => state.formSteps);
    const user = useSelector(state => state.userSlice);
    const foundationFinal = useSelector(state => state.userDonate);
    const { message, isVisible } = useSelector(state => state.modalStore);

    const sendContributionHandler = () => {
        dispatch(sendContribution({
            name: user.name,
            surname: user.surname,
            email: user.email,
            phonenumber: user.phonenumber,
            shelterID: foundationFinal.shelterID,
            price: foundationFinal.price,
        }));
    };

    const allowPersonalInfo = () => {
        if (allowPersonalInfo === true) {
            dispatch(formStepsActions.setUserInfo({allowUserInfo: false}));
        } else {
            dispatch(formStepsActions.setUserInfo({allowUserInfo: true}));
        }
    };

    return (
        <React.Fragment>
            <div className={styles.result}>
                <ResultItem headline="Akou formou chcem pomôcť" text={foundation}/>
                <ResultItem headline="Najviac mi záleží na útulku" text={shelter}/>
                <ResultItem headline="Suma ktorou chcem pomôcť" text={price + ' €'}/>
                <ResultItem headline="Meno a priezvisko" text={name + ' ' + surname}/>
                <ResultItem headline="E-mailová adresa" text={email}/>
                <ResultItem headline="Telefónne číslo" text={phonenumber}/>
                <div className={styles.accept}>
                    <input type="checkbox" value={allowUserInfo} onChange={allowPersonalInfo}/>
                    <p>Súhlasím so spracovaním mojich osobných údajov</p>
                </div>
                <div className={styles.buttons}>
                    <button className={styles.back} type="button" onClick={backBtnHandler}>Späť</button>
                    <button className={`${allowUserInfo === true ? styles.active : styles.send}`}
                            type="button" 
                            onClick={sendContributionHandler} 
                            disabled={!allowUserInfo}>
                        Odoslať formulár
                    </button>
                </div>
            </div>
            {isVisible && <Modal message={message}/>}
        </React.Fragment>
    );
};

export default Result;