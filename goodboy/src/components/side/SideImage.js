import styles from './SideImage.module.css';
import realdog from '../../assets/images/realdog.svg';

const SideImage = () => {
    return (
        <div className={styles.sideImage}>
            <img src={realdog} alt="real-dog"/>
        </div>
    );
};

export default SideImage;