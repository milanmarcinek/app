import styles from './SideHeader.module.css';
import fb from '../../assets/images/fb.svg';
import ig from '../../assets/images/ig.svg';


const SideHeader = () => {
    return (
        <div className={styles.navbar}>
            <div className={styles.main}>
                <div className={styles.nav}>
                    <p>Nadácia Good Boy</p>
                </div>
                <div className={styles.icons}>
                    <img src={fb} alt="fb"/>
                    <img src={ig} alt="ig"/>
                </div>
            </div>
        </div>
    );
};

export default SideHeader;