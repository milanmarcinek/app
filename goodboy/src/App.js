import React from 'react';
import { useDispatch } from 'react-redux';
import './App.css';
import Footer from './components/footer/Footer';
import Main from './components/main/Main';
import SideHeader from './components/side/SideHeader';
import { getShelters } from './store/http-actions';

function App() {

  const dispatch = useDispatch();
  dispatch(getShelters());

  return (
    <React.Fragment>
      <SideHeader/>
      <Main/>
      <Footer/>
    </React.Fragment>
  );
}

export default App;
